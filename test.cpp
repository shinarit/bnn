#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "bnn.hpp"
#include "battleship.hpp"
#include "dummybnn.hpp"

#include <iostream>
#include <cstdlib>
#include <vector>
#include <array>
#include <sstream>
#include <limits>
#include <fstream>
#include <random>

#include <cstdlib>

TEST_CASE("empty network")
{
  const int in(5);
  const int mid(100);
  const int out(5);
  const int steps(100);
  
  bnn network(in, mid, out);
  ::std::vector<int> input;
  input.assign(in, 256);
  for (int i(0); i < steps; ++i)
  {
    network.step(input);
    for (int j(0); j < out; ++j)
    {
      CHECK(0 == network.getOutput(j));
    }
  }
}

TEST_CASE("save and load network")
{
  const int in(5);
  const int mid(100);
  const int out(5);
  const int steps(100);

  bnn network(in, mid, out);
  ::std::mt19937 mt((::std::random_device())());
  network.mutate(mt);
  ::std::vector<int> input;
  input.assign(in, 256);
  ::std::array<::std::array<int, out>, steps> storage;
  bool allZero = true;
  for (int i(0); i < steps; ++i)
  {
    network.step(input);
    for (int j(0); j < out; ++j)
    {
      if (0 != (storage[i][j] = network.getOutput(j)))
      {
        allZero = false;
      }
    }
  }
  CHECK(!allZero);
  
  ::std::stringstream networkStorage;
  network.save(networkStorage);
  
  bnn restoredNetwork(networkStorage);
  for (int i(0); i < steps; ++i)
  {
    restoredNetwork.step(input);
    for (int j(0); j < out; ++j)
    {
      CHECK (storage[i][j] == restoredNetwork.getOutput(j));
    }
  }
}

TEST_CASE("battleship initiating with incorrectg AIs")
{
  const int width(2);
  const int height(2);
  const int correctIn(width * height + 1);
  const int mid(10);
  const int correctOut(1 + 1);
  ::std::mt19937 randomEngine;
  
  bnn correctPlayer(correctIn, mid, correctOut);
  bnn incorrectInputPlayer(correctIn + 1, mid, correctOut);
  bnn incorrectOutputPlayer(correctIn, mid, correctOut + 1);
  bnn totallyIncorrectPlayer(correctIn + 1, mid, correctOut + 1);

  SECTION("both players right")
  {
    CHECK_NOTHROW(Battleship({width, height}, correctPlayer, correctPlayer, randomEngine));
  }
  SECTION("one player incorrect in input")
  {
    CHECK_THROWS_AS(Battleship({width, height}, incorrectInputPlayer, correctPlayer, randomEngine), int);
  }
  SECTION("one player incorrect in output")
  {
    CHECK_THROWS_AS(Battleship({width, height}, incorrectOutputPlayer, correctPlayer, randomEngine), int);
  }
  SECTION("one player incorrect in both")
  {
    CHECK_THROWS_AS(Battleship({width, height}, totallyIncorrectPlayer, correctPlayer, randomEngine), int);
  }
  SECTION("other player incorrect")
  {
    CHECK_THROWS_AS(Battleship({width, height}, correctPlayer, totallyIncorrectPlayer, randomEngine), int);
  }
}

TEST_CASE("battleship nothing happens with empty networks")
{
  const int width(2);
  const int height(2);
  const int in(width * height + 1);
  const int mid(10);
  const int out(2);
  const int steps(1000);
  ::std::mt19937 randomEngine;

  bnn playerOne(in, mid, out);
  bnn playerTwo(in, mid, out);
  
  ::std::stringstream log;
  Battleship bs({width, height}, playerOne, playerTwo, randomEngine, &log);
  CHECK(Battleship::Result::ONGOING == bs.step());
  log.ignore(::std::numeric_limits<::std::streamsize>::max());
  CHECK(Battleship::Result::ONGOING == bs.step());
  ::std::string secondRound(log.str());
  for (int i(0); i < steps - 2; ++i)
  {
    log.ignore(::std::numeric_limits<::std::streamsize>::max());
    CHECK(Battleship::Result::ONGOING == bs.step());
    CHECK(secondRound == log.str());
  }
}
/*
TEST_CASE("battleship draw by ship collision")
{
  const int width(1);
  const int height(1);
  const int in(width * height + 1);
  const int mid(10);
  const int out(2);
  ::std::mt19937 randomEngine;

  bnn playerOne(in, mid, out);
  bnn playerTwo(in, mid, out);
  
  Battleship bs({width, height}, playerOne, playerTwo, randomEngine);
  CHECK(Battleship::Result::DRAW == bs.step());
}

TEST_CASE("shooting kills one ship")
{
  const int width(10);
  const int height(10);
  const int in(width * height + 1);
  const int mid(0);
  const int out(2);
  ::std::mt19937 randomEngine;

  DummyBnn playerOne(in, out);
  bnn playerTwo(in, mid, out);
  
  playerOne.mFutureOut = {{-256, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 256}};

  Battleship bs({width, height}, playerOne, playerTwo, randomEngine);
  for (int i(0); i < 5; ++i)
  {
    CHECK(Battleship::Result::ONGOING == bs.step());
  }
  CHECK(Battleship::Result::PLAYER_ONE_WINS == bs.step());
}
*/