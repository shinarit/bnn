CC = g++
CPP = $(CC)

APP_OBJS = bnn.o battleship.o matchmaker.o main.o breeder.o
TEST_OBJS = bnn.o test.o battleship.o
SRCS = bnn.cpp test.cpp battleship.cpp main.cpp matchmaker.cpp breeder.cpp

ifdef SystemRoot
  RM = del /Q
  EXE = *.exe
  DEFS  =
  INCLUDES	= -I.
  #-I$(SDL_HOME)/include
  LIBPATH =
  #-L$(SDL_HOME)/lib
  #LIBS = -lmingw32
  LIBS = 
  #-lSDLmain -lSDL -lBox2D
else
  ifeq ($(shell uname), Linux)
    RM = rm -f
    EXE = physics graphics
    DEFS =
    #-DSTANDALONE -DHAVE_CONFIG_H -DHAVE_GTK2
    INCLUDES = -I.
    LIBS =
    #-lSDL -lBox2D
  endif
endif

CFLAGS += -std=c++17 -Wall -Wextra -Weffc++ -pedantic -g -U__STRICT_ANSI__ -Wno-sign-compare -DPROGRESSIVE_MUTATION -DKEEP_COMPUTER_RESOURCES 

.PHONY: default set-client client clean
default: client

depend: .depend

.depend: $(SRCS)
	rm -f .depend
	$(CPP) $(CFLAGS) -MM $^ > .depend

include .depend
	
app: $(APP_OBJS)
	$(CPP) -o breeding -g $(APP_OBJS) $(LIBS) $(LIBPATH)

test: $(TEST_OBJS)
	$(CPP) -o test -g $(TEST_OBJS) $(LIBS) $(LIBPATH)

%.o: %.cpp
	$(CPP) -c -o $@ $(TYPE_SELECTOR) $(CFLAGS) $(INCLUDES) $<

clear: clean

clean:
	$(RM) *.o $(EXE)

