#include "bnn.hpp"

#include <iostream>
#include <queue>

const double bnn::EdgeMutationChance = 0.25;
const double bnn::ConnectivityMutationChance = 0.1;
const double bnn::NormalDistribution = 10;

bnn::bnn(int inputNodes, int hiddenNodes, int outputNodes)
  : mInputNo(inputNodes)
  , mHiddenNo(hiddenNodes)
  , mOutputNo(outputNodes)
  , mNodes(inputNodes + hiddenNodes + outputNodes)
  , mEdges(inputNodes + hiddenNodes)
{
}

bnn::bnn(::std::istream& input)
{
  load(input);
}

void bnn::step(const ::std::vector<int>& input)
{
  if (input.size() != mInputNo)
  {
    return;
  }

  for (int i(0); i < mNodes.size() - mOutputNo; ++i)
  {
    if (0 < mNodes[i].visited)
    {
      mNodes[i].value /= mNodes[i].visited;
      mNodes[i].visited = 0;
    }
  }

  for (int i(0); i < mOutputNo; ++i)
  {
    mNodes[mInputNo + mHiddenNo + i].value = 0;
    mNodes[mInputNo + mHiddenNo + i].visited = 0;
  }

  ::std::queue<int> queue;
  for (int i(0); i < mInputNo; ++i)
  {
    mNodes[i].value = input[i];
    queue.push(i);
    mNodes[i].visited = 1;
  }

  while (!queue.empty())
  {
    const int currentNodeIndex(queue.front());
    const Node& currentNode(mNodes[currentNodeIndex]);
    for (const Edge& edge: mEdges[currentNodeIndex])
    {
      Node& linkedNode(mNodes[edge.index]);
      linkedNode.value += currentNode.applyWeight(edge.weight);
      if (!outputNode(edge.index) && 0 == linkedNode.visited)
      {
        queue.push(edge.index);
      }
      linkedNode.visited += currentNode.visited;
    }
    queue.pop();
  }
}

int bnn::getOutput(int index)
{
  if (0 == mNodes[mInputNo + mHiddenNo + index].visited)
    return 0;
  return mNodes[mInputNo + mHiddenNo + index].value / mNodes[mInputNo + mHiddenNo + index].visited;
}

void bnn::mutate(::std::mt19937& mt)
{
  ::std::uniform_int_distribution<int> weightDistribution(-256, 256);
  auto weightGenerator(::std::bind(weightDistribution, std::ref(mt)));
  ::std::uniform_real_distribution<double> chanceDistribution(0.0, 1.0);
  auto chanceGenerator(::std::bind(chanceDistribution, std::ref(mt)));
  
  for (int i(0); i < mInputNo + mHiddenNo; ++i)
  {
    EdgeCollection& edges(mEdges[i]);
    if (edges.empty())
    {
      ::std::uniform_int_distribution<int> targetDistribution(mInputNo, mInputNo + mHiddenNo + mOutputNo - 1);
      edges.insert({targetDistribution(mt), weightGenerator()});
    }
    else
    {
      for (Edge& edge: edges)
      {
        ::std::normal_distribution<double> newWeightDistribution(edge.weight, NormalDistribution);
        if (EdgeMutationChance >= chanceGenerator())
        {
          edge.weight = newWeightDistribution(mt);
          if (edge.weight > 256)
          {
            edge.weight = 256;
          }
          if (edge.weight < -256)
          {
            edge.weight = -256;
          }
        }
      }
      if (ConnectivityMutationChance >= chanceGenerator())
      {
        int targetIndex((::std::uniform_int_distribution<int>(mInputNo, mInputNo + targetNodeNumber() - 1))(mt));
        auto iter(edges.find({targetIndex, 0}));
        if (iter == edges.cend())
        {
          edges.insert({targetIndex, weightGenerator()});
        }
        else
        {
          edges.erase(iter);
        }
        //int targetOrdinal((::std::uniform_int_distribution<int>(1, targetNodeNumber() - edges.size()))(mt));
        // shit algorithm but works for sure
        /*
        for (int i(mInputNo); i < targetNodeNumber() + mInputNo; ++i)
        {
          if (edges.end() == edges.find({i, 0}))
          {
            if (0 == --targetOrdinal)
            {
              edges.insert({i, weightGenerator()});
            }
          }
        }
        */
        // "smart" algorithm that is buggy but thankfully we don't need it since it doesn't remove edges
        /*
        int missingTargetsTillEnd(edges.back().index - (mInputNo - 1) - edges.size());
        int targetIndex;
        if (targetOrdinal > missingTargetsTillEnd)
        {
          targetIndex = edges.back().index + (targetOrdinal - missingTargetsTillEnd);
        }
        else
        {
          --targetOrdinal;
          int prev(mInputNo - 1);
          for (const Edge& edge: edges)
          {
            int skippedDistance(edge.index - prev - 1);
            if (skippedDistance > targetOrdinal)
            {
              targetIndex = edge.index - skippedDistance + targetOrdinal;
              break;
            }
            else
            {
              targetOrdinal -= skippedDistance;
              prev = edge.index;
            }
          }
        }
        edges.insert({targetIndex, weightGenerator()});
        */
      }
    }
  }
}

bool operator<(const bnn::Edge& lhs, const bnn::Edge& rhs)
{
  return lhs.index < rhs.index;
}

::std::ostream& operator<<(::std::ostream& out, const bnn::EdgeCollection& edges)
{
  out << edges.size() << ' ';
  for (auto iter(edges.cbegin()); iter != edges.cend(); ++iter)
  {
    const bnn::Edge& edge(*iter);
    out << edge.index << ' ' << edge.weight << ' ';
  }
  return out;
}

::std::istream& operator>>(::std::istream& in, bnn::EdgeCollection& edges)
{
  int num;
  in >> num;
  for (int i(0); i < num; ++i)
  {
    int index, weight;
    in >> index >> weight;
    edges.insert(edges.end(), {index, weight});
  }
  return in;
}

void bnn::save(::std::ostream& output) const
{
  output << mInputNo << ' ' << mHiddenNo << ' ' << mOutputNo << '\n';
  for (int i(0); i < mNodes.size(); ++i)
  {
    if (!outputNode(i))
    {
      output << i << ' ' << mEdges[i] << '\n';
    }
  }
}

void bnn::load(::std::istream& input)
{
  mNodes.clear();
  mEdges.clear();
  
  input >> mInputNo >> mHiddenNo >> mOutputNo;
  mNodes.resize(mInputNo + mHiddenNo + mOutputNo);
  mEdges.resize(mInputNo + mHiddenNo);
  for (int i(0); i < mEdges.size(); ++i)
  {
    int index;
    input >> index;
    if (i != index)
    {
      throw -1;
    }
    input >> mEdges[i];
  }
}

void bnn::clearState()
{
  for (auto& node: mNodes)
  {
    node.value = 0;
    node.visited = 0;
  }
}
