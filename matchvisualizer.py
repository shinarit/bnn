#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tkinter import *
import sys
import itertools

def Max(lhs, rhs):
  if lhs > rhs:
    return lhs
  else:
    return rhs

def Min(lhs, rhs):
  if lhs > rhs:
    return rhs
  else:
    return lhs

def CreateCanvas(grid):
  canvas = Canvas(grid.tk)
  canvas.xview_moveto(0)
  canvas.yview_moveto(0)
  canvas["bg"] = "white"
  canvas["width"] = 800
  canvas["height"] = 600
  
  return canvas

#
# ----------------------------------------------------------------------------------------
#

class Hexgrid():
  def __init__(self, tk, log):
    lines = list(filter(bool, log.split('\n')))
    M, x, y = lines[0].split(' ')
    sizex = int(x)
    sizey = int(y)
    
    self.steps = []
    for line in lines[1:]:
      if "S0" == line[:2]:
        self.steps.append([])
      self.steps[-1].append(line)
    self.currentStep = 1
  
    self.tk = tk

    self.prevButton = Button(self.tk, text="<-", command=self.PrevStage)
    self.prevButton.grid(row=0, column=0, sticky=E)
    self.nextButton = Button(self.tk, text="->", command=self.NextStage)
    self.nextButton.grid(row=0, column=1, sticky=W)
    self.pageLabel = Label(self.tk, text="1/%d" % len(self.steps))
    self.pageLabel.grid(row=1, columnspan=2)
    self.canvas = CreateCanvas(self)
    self.canvas.grid(columnspan=2)
    
    self.tk.bind('<Left>', lambda event: self.PrevStage())
    self.tk.bind('<Right>', lambda event: self.NextStage())
    self.tk.bind('<Home>', lambda event: self.FirstStage())
    self.tk.bind('<End>', lambda event: self.LastStage())
    
    width = int(self.canvas.cget('width'))
    height = int(self.canvas.cget('height'))
    
    self.fieldsize = Min(width / (sizex + 1), height / sizey)
    self.markersize = self.fieldsize / 4
    self.padx = (width - sizex * self.fieldsize) / 4
    self.pady = (height - sizey * self.fieldsize * 3 / 4) / 2

    for x in range(sizex):
      for y in range(sizey):
        pos = self.GetCoords((x, y))
        self.DrawField(pos[0], pos[1], "gray", "%d;%d" % (x, y))

    self.stepObjects = []
    self.VisualizeSteps()

    #self.tk.mainloop()

  def VisualizeSteps(self):
    fillColors = {
      "S0": "green",
      "S1": "red",
      "B": "black"
      }
    for obj in self.stepObjects:
      self.canvas.delete(obj)
    for line in self.steps[self.currentStep - 1]:
      print(line)
      parts = line.split(' ')
      currentPos = [int(x) for x in parts[-1].split(';')]
      coords = self.GetCoords(currentPos)
      self.stepObjects.append(self.canvas.create_oval((coords[0] + self.fieldsize / 2 - self.markersize / 2,
                                                        coords[1] + self.fieldsize / 2 - self.markersize / 2,
                                                        coords[0] + self.fieldsize / 2 + self.markersize / 2,
                                                        coords[1] + self.fieldsize / 2 + self.markersize / 2),
                                                      fill=fillColors[parts[0]]))

  def UpdateLabel(self):
    self.pageLabel.config(text="%d/%d" % (self.currentStep, len(self.steps)))    

  def FirstStage(self):
    if 1 != self.currentStep:
      self.currentStep = 1
      self.UpdateLabel()
      self.VisualizeSteps()      

  def LastStage(self):
    if len(self.steps) != self.currentStep:
      self.currentStep = len(self.steps)
      self.UpdateLabel()
      self.VisualizeSteps()      

  def NextStage(self):
    if self.currentStep < len(self.steps):
      self.currentStep += 1
      self.UpdateLabel()
      self.VisualizeSteps()

  def PrevStage(self):
    if self.currentStep > 1:
      self.currentStep -= 1
      self.UpdateLabel()
      self.VisualizeSteps()

  def DrawField(self, x, y, fill, txt):
    size = self.fieldsize
    self.canvas.create_polygon([(x, y + size/4), (x, y + size*3/4), (x + size/2, y + size), (x + size, y + size*3/4), (x + size, y + size/4), (x + size/2, y)], fill = fill, outline = "black")
    self.canvas.create_text((x + size/2, y + size/2), text = txt)

  def FindPos(self, x, y):
    pos = min((pow(x - self.GetCoords(pos)[0] - self.fieldsize/2, 2) + pow(y - self.GetCoords(pos)[1] - self.fieldsize/2, 2), pos) for pos in self.table.iterkeys())[1]
    if (pow(x - self.GetCoords(pos)[0] - self.fieldsize/2, 2) + pow(y - self.GetCoords(pos)[1] - self.fieldsize/2, 2)) <= pow(self.fieldsize / 2, 2):
      return pos
    else:
      return None

  def FindField(self, pos):
    return self.table.get(pos)

  def GetSteps(self, pos):
    if 0 == pos[1] % 2:
      return ((1, 0), (0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1))
    else:
      return ((1, 0), (0, 1), (1, 1), (-1, 0), (1, -1), (0, -1))

  def GetCoords(self, pos):
    return (self.padx + pos[0]*self.fieldsize + (1 - pos[1]%2)*self.fieldsize/2,
            600 - self.pady - pos[1]*self.fieldsize*3/4)

if __name__ == "__main__":
  tk = Tk()
  Hexgrid(tk, open(sys.argv[1], 'r').read())
  tk.mainloop()
