#include "breeder.hpp"

#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <thread>
//#include "thread.hpp" //threading not supported in current MinGW version
#include <atomic>
#include <limits>

const char DefaultPrompt[] = "> ";

::std::atomic<bool> BreedingInProgress = false;
::std::atomic<bool> StopBreeding = false;

void runBreeding(Breeder& breeder, int steps, int matchLength)
{
  if (0 == steps)
  {
    steps = ::std::numeric_limits<int>::max();
  }
  ::std::cout << "start breeding for " << steps << " steps\n";
  int stepNo(0);
  for (; !StopBreeding && stepNo < steps; ++stepNo)
  {
    breeder.breed(matchLength);
    ::std::cout << stepNo << ". breeding step complete\n";
  }
  ::std::cout << "finished breeding after " << stepNo << " steps\n";
  StopBreeding = false;
  BreedingInProgress = false;
}

int main()
{
  const ::std::map<char, ::std::string> commandDescriptions = {
    {
      {'n', "new project:\n\tn <projectName> <number of hidden nodes> <number to select for breeding> <population> <width> <height>"},
      {'l', "load project:\n\tl <projectName>"},
      {'b', "breed the AIs:\n\tb <length of each match the AIs play> [number of steps to execute, unlimited if omitted]"},
      {'s', "stop the breeding:\n\ts"},
      {'i', "print information:\n\ti"},
      {'x', "exit breeder:\n\tx"},
      {'?', "print this help:\n\t?"}
    }
  };
  Breeder breeder;
  ::std::string prompt(DefaultPrompt);
  bool exit(false);
  while (!exit)
  {
    ::std::cout << prompt;
    ::std::string line;
    ::std::getline(::std::cin, line);
    if (!line.empty())
    {
      bool needDescription(false);
      bool needBreedingInProgressMessage(false);
      switch (line[0])
      {
        case 'n':
        {
          if (BreedingInProgress)
          {
            needBreedingInProgressMessage = true;
            break;
          }
          ::std::istringstream parameters(line);
          char command;
          ::std::string projectName;
          int hiddenNodes;
          int numberToBreed;
          int population;
          int width;
          int height;
          if (parameters >> command >> projectName >> hiddenNodes >> numberToBreed >> population >> width >> height)
          {
            if (breeder.createProject(projectName, hiddenNodes, numberToBreed, population, {width, height}))
            {
              prompt = projectName + DefaultPrompt;
            }
            else
            {
              ::std::cout << "creation project \"" << projectName << "\" failed\n";
              prompt = DefaultPrompt;
            }
          }
          else
          {
            needDescription = true;
          }
          break;
        }
        case 'l':
        {
          if (BreedingInProgress)
          {
            needBreedingInProgressMessage = true;
            break;
          }
          ::std::istringstream parameters(line);
          char command;
          ::std::string projectName;
          if (parameters >> command >> projectName)
          {
            if (breeder.loadProject(projectName))
            {
              prompt = projectName + DefaultPrompt;
            }
            else
            {
              ::std::cout << "loading project \"" << projectName << "\" failed\n";
              prompt = DefaultPrompt;
            }
          }
          else
          {
            needDescription = true;
          }
          break;
        }
        case 'b':
        {
          if (BreedingInProgress)
          {
            needBreedingInProgressMessage = true;
            break;
          }
          ::std::istringstream parameters(line);
          char command;
          int matchLength;
          if (parameters >> command >> matchLength)
          {
            int steps(0);
            parameters >> steps;
            BreedingInProgress = true;
            ::std::thread(runBreeding, ::std::reference_wrapper(breeder), steps, matchLength).detach();
          }
          else
          {
            needDescription = true;
          } 
          break;
        }
        case 's':
        {
          if (BreedingInProgress)
          {
            StopBreeding = true;
          }
          else
          {
            ::std::cout << "no breeding is in progress\n";
          }
          break;
        }
        case 'i':
        {
          ::std::cout << "breeding is " << (BreedingInProgress ? "" : "NOT ") << "in progress\n";
          ::std::cout << "stop breeding is " << (StopBreeding ? "" : "NOT ") << "set\n";
          break;
        }
        case 'x':
          exit = true;
          StopBreeding = true;
          break;
        case '?':
          for (const auto& command: commandDescriptions)
          {
            ::std::cout << command.first << ": " << command.second << '\n';
          }
          break;
        default:
          ::std::cout << "unrecognized command\n";
          break;
      }
      if (needDescription)
      {
        ::std::cout << commandDescriptions.at(line[0]) << '\n';
      }
      if (needBreedingInProgressMessage)
      {
        ::std::cout << "cannot execute: breeding in progress\n";
      }
    }
  }
  while (BreedingInProgress);
}
