#ifndef _MATCHMAKER_HPP_
#define _MATCHMAKER_HPP_

#include "battleship.hpp"
#include "bnn.hpp"

#include <vector>
#include <iosfwd>
#include <random>
#include <mutex>

class Matchmaker
{
public:
  struct Scoring
  {
    int win;
    int draw;
    int lose;
    int timeout;
  };
  Matchmaker(::std::vector<bnn>& controllers, ::std::mt19937& mt, Battleship::Dimensions dimensions, int matchLength, Scoring scoring = {3, 1, 0, 1}, ::std::ostream* logger = 0);
  Matchmaker(const Matchmaker&) = delete;
  Matchmaker& operator=(const Matchmaker&) = delete;

  ::std::vector<int> scoreControllers();

private:
  static const int DefaultNumberOfThreads = 4;
  static const unsigned MatchChunkSize = 100;

  typedef ::std::vector< ::std::pair<int, int>> MatchCollection;
  void runMatches(::std::vector<int>& scores, MatchCollection& matches, ::std::mutex& nextRoundMutex);
  void runMatch(Battleship& bs, int& playerOneScore, int& playerTwoScore, ::std::ostream* logger);

  ::std::vector<bnn>&             mControllers;
  Battleship::Dimensions          mDimensions;
  const int                       mMatchLength;
  Scoring                         mScoring;
  ::std::ostream*                 mLogger;

  ::std::mt19937&                 mRandomEngine;
};

#endif //_MATCHMAKER_HPP_
