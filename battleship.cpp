#include "battleship.hpp"

#include <iostream>

Battleship::Battleship(Dimensions dimensions, ibnn& playerOne, ibnn& playerTwo, ::std::mt19937& mt, ::std::ostream* logger)
  : mDimensions(dimensions)
  , mShips{{0, 0, Direction::NE}, {mDimensions.width - 1, mDimensions.height - 1, Direction::SW}}
  , mControllers{playerOne, playerTwo}
  , mBullets()
  , mLogger(logger)
{
  for (ibnn& network: mControllers)
  {
    if (mDimensions.height * mDimensions.width + 1 != network.inputNumber() ||
        //turn and fire
        1 + 1 != network.outputNumber())
    {
      throw -1;
    }
  }
  //randomize ship starting locations
  ::std::uniform_int_distribution<int> xDistribution(0, mDimensions.width / 2 - 1);
  auto xGenerator(::std::bind(xDistribution, std::ref(mt)));
  ::std::uniform_int_distribution<int> yDistribution(0, mDimensions.height / 2 - 1);
  auto yGenerator(::std::bind(yDistribution, std::ref(mt)));

  mShips[0].x = xGenerator();
  mShips[0].y = yGenerator();
  mShips[1].x = mDimensions.width - 1 - xGenerator();
  mShips[1].y = mDimensions.height - 1 - yGenerator();
  
  if (mLogger)
  {
    *mLogger << "M " << mDimensions.width << ' ' << mDimensions.height << '\n';
  }
}

template <class T, class U>
bool collide(const T& lhs, const U& rhs)
{
  return lhs.x == rhs.x && lhs.y == rhs.y;
}

Battleship::Result Battleship::step()
{
  //get input
  for (int i(0); i < 2; ++i)
  {
    Ship& currentShip(mShips[i]);
    const Ship& otherShip(mShips[1 - i]);
    ::std::vector<int> input(mControllers[0].get().inputNumber());
    const int sideMarker(256);
    input[currentShip.y * mDimensions.width + currentShip.x] = sideMarker;
    input[otherShip.y * mDimensions.width + otherShip.x] = -sideMarker;
    for (auto& bullet: mBullets)
    {
      input[bullet.y * mDimensions.width + bullet.x] = -sideMarker / 2;
    }
    int& directionMarker(input.back());
    switch (currentShip.direction)
    {
      case Direction::NW:
        directionMarker = 256;
        break;
      case Direction::NE:
        directionMarker = 171;
        break;
      case Direction::E:
        directionMarker = 86;
        break;
      case Direction::SE:
        directionMarker = 0;
        break;
      case Direction::SW:
        directionMarker = -86;
        break;
      case Direction::W:
        directionMarker = -171;
        break;
    }
    mControllers[i].get().step(input);
    auto turnDirection(Ship::getSide(mControllers[i].get().getOutput(0)));
    if (turnDirection)
    {
      turn(currentShip, turnDirection.value());
    }
    auto shootDirection(Ship::getSide(mControllers[i].get().getOutput(1)));
    if (shootDirection && 0 == currentShip.reloadTimer)
    {
      currentShip.reloadTimer = Ship::RELOAD_TIME;
      mBullets.push_back({currentShip.x, currentShip.y, currentShip.direction, Bullet::LIFETIME});
      turn(mBullets.back(), shootDirection.value());
    }
  }

  //move shit
  for (int i(0); i < 2; ++i)
  {
    if (mShips[i].reloadTimer)
      --mShips[i].reloadTimer;
    int prevX(mShips[i].x);
    int prevY(mShips[i].y);
    int origX(mShips[i].x);
    int origY(mShips[i].y);
    if (move(mShips[i]))
    {
      mShips[i].x = origX;
      mShips[i].y = origY;
    }
    if (mLogger)
    {
      *mLogger << "S" << i << ' ' << mShips[i].reloadTimer
              << ' ' << prevX << ';' << prevY << " -> "
              << mShips[i].x << ';' << mShips[i].y << '\n';
    }
  }
  if (collide(mShips[0], mShips[1]))
    return Result::DRAW;

  bool died[] = {false, false};
  for (int i(0); i < mBullets.size(); ++i)
  {
    Bullet& bullet(mBullets[i]);
    int prevX(bullet.x);
    int prevY(bullet.y);
    bool leftField(move(bullet));
    if (leftField || 0 == --bullet.lifetime)
    {
      mBullets.erase(mBullets.begin() + i);
      --i;
      continue;
    }
    if (mLogger)
    {
      *mLogger << "B " << bullet.lifetime << ' '
              << prevX << ';' << prevY << " -> "
              << bullet.x << ';' << bullet.y << '\n';
    }
    //collision check shit
    for (int i(0); i < 2; ++i)
    {
      if (collide(mShips[i], bullet))
        died[i] = true;
    }
  }
  if (died[0] || died[1])
  {
    if (died[0] && died[1])
      return Result::DRAW;
    if (died[0])
      return Result::PLAYER_TWO_WINS;
    if (died[1])
      return Result::PLAYER_ONE_WINS;
  }

  return Result::ONGOING;
}

template <class T>
void turn(T& object, Battleship::Side side)
{
  Battleship::Direction directionVector[6][2] = 
  {
    {
      Battleship::Direction::W, Battleship::Direction::NE
    },
    {
      Battleship::Direction::NW, Battleship::Direction::E
    },
    {
      Battleship::Direction::NE, Battleship::Direction::SE
    },
    {
      Battleship::Direction::E, Battleship::Direction::SW
    },
    {
      Battleship::Direction::SE, Battleship::Direction::W
    },
    {
      Battleship::Direction::SW, Battleship::Direction::NW
    }
  };
  
  object.direction = directionVector[static_cast<int>(object.direction)][static_cast<int>(side)];
}

::std::optional<Battleship::Side> Battleship::Ship::getSide(int input)
{
  if (input > 32)
    return Side::LEFT;
  else if (input < -32)
    return Side::RIGHT;
  return ::std::optional<Side>();
}
