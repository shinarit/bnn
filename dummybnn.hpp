#ifndef _DUMMYBNN_HPP_
#define _DUMMYBNN_HPP_

#include "ibnn.hpp"

class DummyBnn
  : public ibnn
{
public:
  DummyBnn(int in, int out)
    : mIn(in)
    , mOut(out)
    , mPlannedOut(mOut)
    , mFutureOut()
  {
  }
  
  virtual int inputNumber() const
  {
    return mIn;
  }
  virtual int outputNumber() const
  {
    return mOut;
  }
    
  virtual void step(const ::std::vector<int>& input)
  {
    if (!mFutureOut.empty())
    {
      mPlannedOut = mFutureOut[0];
      mFutureOut.erase(mFutureOut.begin());
    }
  }
  virtual int getOutput(int index)
  {
    return mPlannedOut[index];
  }
  
  int mIn;
  int mOut;
  typedef ::std::vector<int> OutputVector;
  OutputVector mPlannedOut;
  ::std::vector<OutputVector> mFutureOut;
};

#endif //_DUMMYBNN_HPP_
