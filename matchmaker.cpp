#include "matchmaker.hpp"

#include "battleship.hpp"

#include <iostream>
#include <thread>
//#include "thread.hpp" //threading not supported in current MinGW version
#include <sstream>

Matchmaker::Matchmaker(::std::vector<bnn>& controllers, ::std::mt19937& mt, Battleship::Dimensions dimensions, int matchLength, Scoring scoring, ::std::ostream* logger)
  : mControllers(controllers)
  , mDimensions(dimensions)
  , mMatchLength(matchLength)
  , mScoring(scoring)
  , mLogger(logger)
  , mRandomEngine(mt)
{ }

::std::vector<int> Matchmaker::scoreControllers()
{
  //::std::mutex scoringMutex;
  //::std::mutex loggingMutex;
  MatchCollection matchesToPlay;
  matchesToPlay.reserve(mControllers.size() * (mControllers.size() - 1));
  for (int i(0); i < int(mControllers.size()) - 1; ++i)
  {
    for (int j(i + 1); j < mControllers.size(); ++j)
    {
      matchesToPlay.push_back({i, j});
    }
  }
  ::std::mutex nextRoundMutex;
  ::std::vector<int> score(mControllers.size());
  
  int numberOfThreads(::std::thread::hardware_concurrency());
  if (0 == numberOfThreads)
  {
    numberOfThreads = DefaultNumberOfThreads;
  }
#ifdef KEEP_COMPUTER_RESOURCES
  if (numberOfThreads > 1)
  {
    --numberOfThreads;
  }
#endif
  ::std::vector<::std::thread> threads;
  for (int i(0); i < numberOfThreads; ++i)
  {
    threads.emplace_back(::std::thread(
      &Matchmaker::runMatches, this, ::std::ref(score), ::std::ref(matchesToPlay), ::std::ref(nextRoundMutex)));
  }
  
  for (int i(0); i < threads.size(); ++i)
  {
    threads[i].join();
  }
  return score;
}

void Matchmaker::runMatches(::std::vector<int>& scores, MatchCollection& matches, ::std::mutex& nextRoundMutex)
{
  MatchCollection myChunk;
  {
    ::std::lock_guard< ::std::mutex> lock(nextRoundMutex);
    int numberToMove(::std::min<int>(matches.size(), MatchChunkSize));
    myChunk.assign(matches.begin() + matches.size() - numberToMove, matches.end());
    matches.erase(matches.begin() + matches.size() - numberToMove, matches.end());
  }
  ::std::vector<bnn> localControllers(mControllers);
  
  while (!myChunk.empty())
  {
    ::std::ostringstream log;
    ::std::vector<int> localScores(scores.size());
    for (auto match: myChunk)
    {
      if (mLogger)
        log << match.first << " vs " << match.second << '\n';
      {
        Battleship bs(mDimensions, localControllers[match.first], localControllers[match.second], mRandomEngine, mLogger ? &log : 0);
        runMatch(bs, localScores[match.first], localScores[match.second], mLogger ? &log : 0);
        localControllers[match.first].clearState();
        localControllers[match.second].clearState();
      }
      if (mLogger)
        log << match.second << " vs " << match.first << '\n';
      {
        Battleship bs(mDimensions, localControllers[match.second], localControllers[match.first], mRandomEngine, mLogger ? &log : 0);
        runMatch(bs, localScores[match.second], localScores[match.first], mLogger ? &log : 0);
        localControllers[match.first].clearState();
        localControllers[match.second].clearState();
      }
    }
    
    ::std::lock_guard< ::std::mutex> lock(nextRoundMutex);
    int numberToMove(::std::min<int>(matches.size(), MatchChunkSize));
    myChunk.assign(matches.begin() + matches.size() - numberToMove, matches.end());
    matches.erase(matches.begin() + matches.size() - numberToMove, matches.end());
    if (mLogger)
      *mLogger << log.str();
    for (int i(0); i < localScores.size(); ++i)
    {
      scores[i] += localScores[i];
    }
  }
}

void Matchmaker::runMatch(Battleship& bs, int& playerOneScore, int& playerTwoScore, ::std::ostream* logger)
{
  for (int k(0); k < mMatchLength; ++k)
  {
    switch (bs.step())
    {
      case Battleship::Result::PLAYER_ONE_WINS:
        playerOneScore += mScoring.win;
        playerTwoScore += mScoring.lose;
        if (logger)
          *logger << "p1 wins in " << k << " steps\n";
        return;
      case Battleship::Result::PLAYER_TWO_WINS:
        playerOneScore += mScoring.lose;
        playerTwoScore += mScoring.win;
        if (logger)
          *logger << "p2 wins in " << k << " steps\n";
        return;
      case Battleship::Result::DRAW:
        playerOneScore += mScoring.draw;
        playerTwoScore += mScoring.draw;
        if (logger)
          *logger << "draw in " << k << " steps\n";
        return;
      case Battleship::Result::ONGOING:
        break;
    }
  }
  if (logger)
    *logger << "timeout after " << mMatchLength << " steps\n";
  playerOneScore += mScoring.timeout;
  playerTwoScore += mScoring.timeout;
}
