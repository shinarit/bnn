#ifndef _BNN_HPP_
#define _BNN_HPP_

#include "ibnn.hpp"

#include <iosfwd>
#include <flatset.hpp>
#include <random>

class bnn
  : public ibnn
{
public:
  bnn(int inputNodes, int hiddenNodes, int outputNodes);
  bnn(::std::istream& input);
  
  virtual int inputNumber() const { return mInputNo; }
  virtual int outputNumber() const { return mOutputNo; }
    
  virtual void step(const ::std::vector<int>& input);
  virtual int getOutput(int index);
  
  void mutate(::std::mt19937& mt);
  void save(::std::ostream& output) const;
  void load(::std::istream& input);
  void clearState();

private:
  static const double EdgeMutationChance;
  static const double ConnectivityMutationChance;
  static const double NormalDistribution;
  //
  // weight is [-256 .. 256]
  //
  struct Node
  {
    int value = 0;
    int visited = 0;
    int applyWeight(int weight) const
    {
      return (value * weight) >> 8;
    }
  };
  struct Edge
  {
    int index;
    int weight;
  };
  friend bool operator<(const Edge& lhs, const Edge& rhs);
  
  typedef ::flat::flat_set<Edge> EdgeCollection;
  
  friend ::std::ostream& operator<<(::std::ostream& out, const EdgeCollection& edges);
  friend ::std::istream& operator>>(::std::istream& in, EdgeCollection& edges);
    
  bool outputNode(int index) const
  {
    return index >= mInputNo + mHiddenNo;
  }
  int targetNodeNumber() const
  {
    return mHiddenNo + mOutputNo;
  }
  
  int                                     mInputNo;
  int                                     mHiddenNo;
  int                                     mOutputNo;
  
  ::std::vector<Node>                     mNodes;
  ::std::vector<EdgeCollection>           mEdges;
};

#endif //_BNN_HPP_
