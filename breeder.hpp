#ifndef _BREEDER_HPP_
#define _BREEDER_HPP_

#include "battleship.hpp"
#include "bnn.hpp"
#include "matchmaker.hpp"

#include <vector>
#include <random>
#include <string>

//
//  project:
//  projectname/
//    descriptor
//      width height bestNtoselectforbreeding
//    00000000/
//      networks/
//        0000
//        0001
//        ...
//        N
//      matches
//    00000001/
//    ...
//    N/
//

class Breeder
{
public:
  Breeder();
  
  bool createProject(::std::string name, int numberOfHiddenNodes, int numberToBreed, int population, Battleship::Dimensions dimensions);
  bool loadProject(::std::string name);
  
  void breed(int matchLength);

private:
  static const int NetworkFileNameLength = 4;
  static const int StepDirectoryNameLength = 8;
  
  bool saveNetworks(int step);
  bool loadNetworks(int step);
  
  ::std::string generateStepPath(int step) const;

  ::std::mt19937            mRandomEngine;
  
  ::std::string             mProjectName;
  ::std::vector<bnn>        mCurrentBreed;
  Battleship::Dimensions    mDimensions;
  int                       mNumberToSelect;
  Matchmaker::Scoring       mScoring;
  
  int                       mCurrentStep;
};

#endif //_BREEDER_HPP_
