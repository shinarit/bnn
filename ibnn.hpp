#ifndef _IBNN_HPP_
#define _IBNN_HPP_

#include <vector>

class ibnn
{
public:
  virtual ~ibnn()
  { }
  virtual int inputNumber() const = 0;
  virtual int outputNumber() const = 0;
    
  virtual void step(const ::std::vector<int>& input) = 0;
  virtual int getOutput(int index) = 0;
};

#endif //_IBNN_HPP_