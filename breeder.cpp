#include "breeder.hpp"

#include "bnn.hpp"

#include <windows.h>

#include <sstream>
#include <fstream>
#include <chrono>
#include <cstdlib>
#include <algorithm>

Breeder::Breeder()
  : mRandomEngine(::std::chrono::system_clock::now().time_since_epoch().count())
  , mProjectName()
  , mCurrentBreed()
  , mDimensions()
  , mNumberToSelect()
  , mScoring{10, 3, -3, -5}
  , mCurrentStep()
{ }

bool Breeder::createProject(::std::string name, int numberOfHiddenNodes, int numberToBreed, int population, Battleship::Dimensions dimensions)
{
  if (CreateDirectory(name.c_str(), NULL))
  {
    mProjectName = name;
    mDimensions = dimensions;
    mNumberToSelect = numberToBreed;
    mCurrentStep = 0;
    ::std::ofstream descriptor((name + "\\descriptor").c_str());
    if (!descriptor.is_open())
    {
      return false;
    }
    descriptor  << dimensions.width << ' ' << dimensions.height << ' '
                << mNumberToSelect << '\n';
    mCurrentBreed.clear();
    mCurrentBreed.reserve(population);
    for (int i(0); i < population; ++i)
    {
      mCurrentBreed.push_back(bnn(mDimensions.width * mDimensions.height + 1, numberOfHiddenNodes, 2));
      mCurrentBreed.back().mutate(mRandomEngine);
    }
    return saveNetworks(mCurrentStep);
  }
  else
  {
    return false;
  }
}
bool Breeder::loadProject(::std::string name)
{
  mProjectName = name;
  ::std::ifstream descriptor((name + "\\descriptor").c_str());
  if (!descriptor.is_open())
  {
    return false;
  }
  
  if (!(descriptor >> mDimensions.width >> mDimensions.height >> mNumberToSelect))
  {
    return false;
  }

  //find the current step
  {
    WIN32_FIND_DATA findFileData;
    HANDLE findHandle = INVALID_HANDLE_VALUE;

    findHandle = FindFirstFile((LPCSTR)(name + "\\*").c_str(), &findFileData);
    if (findHandle == INVALID_HANDLE_VALUE)
    {
      return false;
    }
    int currentMax(-1);
    do
    {
      if (0 != (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
      {
        const int stepNumber(::std::atoi(findFileData.cFileName));
        if (stepNumber > currentMax)
        {
          currentMax = stepNumber;
        }
      }
    } 
    while (FindNextFile(findHandle, &findFileData) != 0);
    if (-1 == currentMax)
    {
      return false;
    }
    mCurrentStep = currentMax;
  }
  
  //load the AIs
  return loadNetworks(mCurrentStep);
}

namespace
{
struct SortingPair
{
  bnn* network;
  int score;
};
}

void Breeder::breed(int matchLength)
{
  ::std::ofstream breedingLogs((generateStepPath(mCurrentStep) + "\\breedinglogs").c_str());
  ::std::ofstream matchLogs((generateStepPath(mCurrentStep) + "\\matches").c_str());
  
  breedingLogs << "breeding " << mCurrentBreed.size() << " networks with matchlength " << matchLength << '\n';

  Matchmaker matchmaker(mCurrentBreed, mRandomEngine, mDimensions, matchLength, mScoring, &matchLogs);
  ::std::vector<int> scores(matchmaker.scoreControllers());
  ::std::vector<SortingPair> sortingVector;
  sortingVector.reserve(scores.size());
  breedingLogs << "final scoring:\n";
  for (int i(0); i < scores.size(); ++i)
  {
    breedingLogs << "  " << i << ": " << scores[i] << '\n';
    sortingVector.push_back({&mCurrentBreed[i], scores[i]});
  }
  ::std::sort(begin(sortingVector), end(sortingVector), [](const SortingPair& lhs, const SortingPair& rhs) { return lhs.score > rhs.score; });
  
  ::std::vector<bnn> newBreed;
  newBreed.reserve(mCurrentBreed.size());
  int numberToSelect(sortingVector.front().score != sortingVector.back().score ? mNumberToSelect : mCurrentBreed.size());
  for (int i(0); i < mCurrentBreed.size(); ++i)
  {
    const SortingPair& pairToBreed(sortingVector[i % numberToSelect]);
    breedingLogs << "breeding " << pairToBreed.network - &mCurrentBreed[0] << " to " << newBreed.size() << '\n';
    newBreed.push_back(*pairToBreed.network);
#ifdef PROGRESSIVE_MUTATION
    for (int j(0); j < i / numberToSelect; ++j)
#endif
    newBreed.back().mutate(mRandomEngine);
  }
  mCurrentBreed.swap(newBreed);
  
  saveNetworks(++mCurrentStep);
}

bool Breeder::saveNetworks(int step)
{
  ::std::string stepPath(generateStepPath(step));
  if (!CreateDirectory(stepPath.c_str(), NULL)
      && ERROR_ALREADY_EXISTS != GetLastError())
  {
    return false;
  }
  ::std::string basePath(stepPath + "\\networks");
  if (!CreateDirectory(basePath.c_str(), NULL)
      && ERROR_ALREADY_EXISTS != GetLastError())
  {
    return false;
  }

  for (int i(0); i < mCurrentBreed.size(); ++i)
  {
    ::std::ostringstream fileNameAssembler;
    fileNameAssembler.width(NetworkFileNameLength);
    fileNameAssembler.fill('0');
    fileNameAssembler << i;
    
    ::std::ofstream file((basePath + "\\" + fileNameAssembler.str()).c_str());
    if (!file.is_open())
    {
      return false;
    }
    mCurrentBreed[i].save(file);
  }
  return true;
}

bool Breeder::loadNetworks(int step)
{
  mCurrentBreed.clear();
  WIN32_FIND_DATA findFileData;
  HANDLE findHandle = INVALID_HANDLE_VALUE;
  
  ::std::string path(generateStepPath(step) + "\\networks");

  findHandle = FindFirstFile((LPCSTR)((path + "\\*").c_str()), &findFileData);
  if (findHandle == INVALID_HANDLE_VALUE)
  {
    return false;
  }
  do
  {
    if (0 == (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
    {
      ::std::ifstream aiFile((path + "\\" + findFileData.cFileName).c_str());
      if (!aiFile.is_open())
      {
        return false;
      }
      mCurrentBreed.push_back(bnn(aiFile));
    }
  }
  while (FindNextFile(findHandle, &findFileData) != 0);

  return true;
}

::std::string Breeder::generateStepPath(int step) const
{
  ::std::ostringstream stepPathAssembler;
  stepPathAssembler << mProjectName << '\\';
  stepPathAssembler.width(StepDirectoryNameLength);
  stepPathAssembler.fill('0');
  stepPathAssembler << step;
  return stepPathAssembler.str();
}
